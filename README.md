# yCuda - A Cuda wrapper for the Yorick programming language

yCuda provides an interface that allows the use of an Nvidia GPU as an accelerator within yorick. A cuda context and memory space is maintained by a daemon process that has been written in C++11.

yCuda has been writen as a Linux application using POSIX shared memory and Linux signals. Data is shared between yorick and the yCuda daemon using POSIX shared memory and three data structures. 
* A shared heap struct is maintained for the purpose of moving data between yorick and the daemon (Currently there are unnecessary data copies as data is moved to the heap and then copied to the daemon. A managed heap would improve performance.). 
* A shared command struct is used to communicate the intention of the user with the daemon. This contains information on command type, data type, memory keys, a data pointer, function and module names, etc. See `src/mem.h`
* A shared status struct is maintained to track what the daemon is doing. This is required for synchonization.

The SIGUSR1 signal is used to wake the yCuda daemon. On waking the daemon will interpret the shared command struct. The SIGUSR2 signal has been used to synchronize the yCuda daemon when necessary. This will call `cuCtxSynchronize()` without interpreting the shared memory.

(Although this has been written with yorick in mind, the daemon is language agnostic and should be usable once the API is adhered to.)

## Installation
Unpack the source and change directory to where it's unpacked and run:

```
  make
```

This will build the daemon. To install the yorick interface, run:

```
  cd yorick
  yorick -batch make.i
  make clean
  make
  make install
```

This will build the yorick interface and move the daemon to `"Y_HOME"/lib/`. This is required as this is where yorick will expect the daemon binary to be when launching it.

**Note:** The yCuda daemon Makefile links with libcuda. This requires the flags `-I/usr/local/cuda/include`, `-L/usr/local/cuda/lib64` and `-lcuda`. If required, edit the library and include paths.

**Todo:** Check if yorick Makefile needs Cuda linking, I don't think it does

## Yorick functions
#### `cuInit()`
Launches the yCuda daemon and returns its PID. The PID required for signalling the daemon.

#### `cuKill()`
Kills the daemon cleanly, releasing all resources that were acquied during its lifetime.

#### `cuSync()`
Signals the daemon with SIGUSR2 to call for a context sychronization.

## Yorick/yCuda interface function list
* `cuArray(&input, ..)`
* `cuArrayDelete(&input, ..)`
* `cuArraySet(&input)`
* `cuArrayGet(&input)`
* `cuArrayQuery(&input)`
* `cuModuleCompile(path)`
* `cuModuleLoad(path)`
* `cuModuleUnload(path)`
* `cuFunctionLoad(name, path)`
* `cuFunctionUnload(name)`
* `cuKernelLaunch(kernelDims, name, cuArgs(args...))`

**Note:** Functions with `&input` as an argument require the passing of a yorick pointer. The pointer value is used as a memory key to uniquely identify each variable within the daemon's managed memory. The yorick pointer is unused otherwise as they are incompatible with standard C-style pointers.

**Note:** Functions with `..` arguments are variadic.

**Note:** `path` arguments *must* be absolute paths

## Yorick/yCuda interface functions explained

#### `cuArray(&input, ..)`
Takes a yorick pointer as an input and copies the content of the array to both the daemon and GPU device.

#### `cuArrayDelete(&input, ..)`
**TODO:** Check if this function is safe when object is unallocated

**TODO:** Create function to check all yorick variables against daemon managed memory. This could free dangling pointers

Frees an allocated memory item from both the daemon and GPU device.

#### `cuArraySet(&input)`
**~~Potentially unsafe..?~~ Completely broken**
Updates an already allocated memory item with the new values of input, reallocating memory as needed. If the memory item isn't already allocated, a new item is created.

#### `cuArrayGet(&input)`
**TODO:** Check return values, is it okay to, for example `c = cuArrayGet(&c)`

Returns an array, in place, from the GPU device.

#### `cuArrayQuery(&input)`
Returns an array of size `Y_DIMSIZE+2 ( = 13)` of the form
`{Data type, number of elements, rank, dimension size[0], ..., dimension size[9]}`. This array gives the user information on the GPU device array `&input`.

#### `cuModuleCompile(path)`
Cuda kernels must be written following the driver API rather than the runtime API. The kernels must be written in a .cu file that is then compiled to a .ptx module using nvcc. Passing an absolute path of a .cu file to this functio will compile it to a .ptx module in the same directory.

#### `cuModuleLoad(path)`
Passing an absolute path of a .ptx module to this function will load that module within the yCuda daemon. This function also accepts paths to .cu files and will compile them if there is no corresponding .ptx module in the same directory.

#### `cuModuleUnload(path)`
Unloads a loaded .ptx module from the yCuda daemon.

#### `cuFunctionLoad(name, path)`
Loads the function, given by `name`, in module `path` within the yCuda daemon. The function is then referenced by its name as it is used as the key to a C++ map containing all loaded function pointers.

#### `cuFunctionUnload(name)`
Unloads a loaded function from the yCuda daemon.

#### `cuKernelLaunch(kernelDims, name, cuArgs(args, ..))`
Launches a Cuda kernel on the GPU device. `kernelDims` must be passed as a yorick array of the form `[gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ]`. Arguments to the kernel must be passed through the variadic function `cuArgs(args, ..)`. The `cuArgs` function encodes its arguments such that device arrays are passed to the yCuda daemon as memory keys and constants are passed as constants. The encoding is generic and works with any plain data type. Constants are encoded by assigning their value to the relevant element of a C++ union and passing the `long` element to the daemon. Each argument is also assigned a second value to distinguish memory keys from constants and also to encode the correct data type of constants.


## Example usage
```c
#include "yCuda.i"
len = 1000;
cuInit;
a = array(float(1),len);
b = array(float(2),len);
c = array(float(0),len);
cuArray, &a, &b, &c;
mod = Y_HOME+"testKernels/vector.cu";
ptx = Y_HOME+"testKernels/vector.ptx";
function = "vecAdd_f";
cuModuleCompile, mod;
cuModuleLoad, ptx;
cuFunctionLoad, function, ptx;
blockX = 16; gridX = (len-1)/blockX + 1;
blockY = 16; gridY = (len-1)/blockY + 1;
cuKernelLaunch([gridX,gridY,1,blockX,blockY,1], function, cuArgs(&a, &b, &c, len));
cuArrayGet, &c;
cuFunctionUnload, function;
cuModuleUnload, ptx;
cuKill;
```
