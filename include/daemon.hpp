#ifndef DAEMON_HPP
#define DAEMON_HPP

static void handler(int sig);

static void shmHeapCleanup();
static void shmCommandCleanup();
static void shmStatusCleanup();
static void cudaCleanup();

static void interpretSharedCommand(struct yCudaCommandStruct *command);

#endif /* DAEMON_HPP */
