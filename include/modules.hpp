#ifndef MODULES_H
#define MODULES_H

#include <string>        // std::string
//#include <unistd.h>      // ftruncate
#include <sys/stat.h>    // struct stat, stat()
#include <unordered_map> // std::unordered_map
#include <cuda.h>        // CUmodule, cuModuleLoad(), cuModuleUnload()

class cudaModules
{
private:
	std::unordered_map<std::string, CUmodule*> moduleMap;

	void modulePathToName(std::string& modulePath);
	std::string cuToPtx(const std::string& modulePath);

public:
	//cudaModules();
	~cudaModules();

	void loadModule(std::string modulePath);
	void unloadModule(std::string modulePath);
	CUmodule* getModule(std::string modulePath);
};

#endif /* MODULES_H */
