#ifndef COMMON_HPP
#define COMMON_HPP

#define PACKAGE_VERSION "0.10"

//#ifdef __cplusplus
//extern const char *shmHeapFile;
//extern const char *shmCommandFile;
//extern const char *shmStatusFile;
//extern const char *shmPIDFile;
//#else
const char *shmHeapFile    = "yCudaSharedHeap";
const char *shmCommandFile = "yCudaSharedCommand";
const char *shmStatusFile  = "yCudaSharedStatus";
const char *shmPIDFile     = "yCudaSharedPID";
//#endif

#endif /* COMMON_HPP */
