#ifndef MEMORY_HPP
#define MEMORY_HPP

#ifdef __cplusplus
//#include <algorithm>
#include <unordered_map> // std::unordered_map
#include <typeinfo>      // std::type_info, typeid
#include <unistd.h>      // ftruncate
#include <cstring>       // std::memset
//#include <sstream>

#include <cuda.h>        // All the CUDA definitions/functions

//#include "modules.hpp"
#include "functions.hpp" // cudaFunctions::getFunction()
#endif //ifdef __cplusplus

// From yapi.h
#define Y_DIMSIZE 11

#define PATH_LENGTH 4096
#define FUNC_LENGTH 255

extern struct shmHeap sharedHeap;
extern struct shmCmd  sharedCommand;
extern volatile struct shmStat sharedStatus;

// Data type enum - Not all implemented {{{
typedef enum dataType {
	CHAR,
	SHORT,
	INT,
	LONG,
	FLOAT,
	DOUBLE,
	COMPLEX,
	STRING,
	POINTER,
	STRUCT,
	VOID	
} dataType;
//}}}
// Command type enum {{{
typedef enum cmdType {
	MEM_NEW,
	MEM_DELETE,
	MEM_GET,
	MEM_SET,
	MEM_QUERY,
	MEM_PURGE,
	MOD_COMPILE,
	MOD_LOAD,
	MOD_UNLOAD,
	FUNC_LOAD,
	FUNC_UNLOAD,
	KERN_LAUNCH
} cmdType;
//}}}
// Daemon status enum {{{
typedef enum daemonStatus {
	DAEMON_WAITING,
	DAEMON_BUSY,
	DAEMON_ERROR // Set on EXIT_FAILURE
} daemonStatus;
//}}}
// Data union type - used for argument encoding in cuArgs {{{
typedef union {
	char c;
	short s;
	int i;
	long l;
	float f;
	double d;
	void *ptr;
} dataUnion;
//}}}

// Command structure - Shared between Yorick and daemon {{{
struct yCudaCommandStruct {
	cmdType cType;          // type of command to exec
	dataType dType;         // type of data pointed to by dataPtr
	long memoryKey;         // key to unordered_map in memoryItemMap class
	void* dataPtr;          // pointer to data in shared heap
	size_t dims[Y_DIMSIZE]; // dimensions of data (for arrays)
	size_t numElements;     // number of elements pointed to by dataPtr
	size_t dataSize;        // number of bytes pointed to by dataPtr
	char modulePath[PATH_LENGTH]; // ABSOLUTE module path
	char funcName[FUNC_LENGTH];   // function name within module
};
//}}}
// Shared Heap {{{
struct shmHeap {
	int fd;         // file descriptor of shared memory segment
	size_t size;    // size of shared memory segment in bytes
	size_t minSize; // minimum size of shared memory segment in bytes
	void* heap;     // pointer to data in shared memory segment
};
//}}}
// Shared Command {{{
struct shmCmd {
	int fd;         // file descriptor of shared memory segment
	size_t size;    // size of shared memory segment in bytes
	struct yCudaCommandStruct *sharedCommandStruct; // pointer to command
};
//}}}
// Shared Status {{{
struct shmStat {
	int fd;         // file descriptor of shared memory segment
	size_t size;    // size of shared memory segment in bytes
	daemonStatus *status; // daemon's current status - idle, busy, etc.
};
//}}}

#ifdef __cplusplus
// Memory management system {{{
// Virtual base class for memoryItem {{{
class memoryBase
{
public:
	memoryBase (std::type_info *inputType) : dataType(inputType) {}
	virtual ~memoryBase() {};

	virtual void setData(void* input, size_t* dims, size_t inputSizeInBytes) = 0;
	virtual void* getData() = 0;
	virtual void* getDevicePtr() = 0;
	virtual size_t getSize() = 0;
	virtual size_t getElems() = 0;
	virtual size_t* getDims() = 0;

	const std::type_info *dataType;
};
//}}}

// memoryItem definition {{{
template <typename T>
class memoryItem : public memoryBase
{
public:
	memoryItem(T* inputData);
	memoryItem(T* inputData, size_t* dims, size_t numElements);
	~memoryItem();

	void setData(void* input, size_t* dims, size_t inputSizeInBytes);
	void* getData();

	void* getDevicePtr() { return (void*)&deviceData; }
	size_t getSize() { return sizeInBytes; }
	size_t getElems() { return sizeInElements; }
	size_t* getDims() { return &dataDimensions[0]; }

private:
	T* data;
	CUdeviceptr deviceData;
	size_t sizeInBytes;
	size_t sizeInElements;
	size_t dataDimensions[Y_DIMSIZE];
};
//}}}

//TODO: Make a singleton?
// memoryItemMap: Class to manage constructed memoryItems {{{
class memoryItemMap
{
public:
	~memoryItemMap();

	void newItem(long key, dataType type);
	void newItem(long key, dataType type, void* dataPtr, size_t* dims, size_t numElements);
	void setItem(long key, dataType type, void* dataPtr, size_t* dims, size_t numElements);
	void getItem(long key, dataType* type, void* destPtr, size_t* dims, size_t* numElements, size_t* dataSize);
	void queryItem(long key, dataType* type, size_t* dims, size_t* numElements);
	void purgeItems();

	void* getDevicePtr(long key) { return memoryMap[key]->getDevicePtr(); }
	void deleteItem(long key) { delete memoryMap[key]; memoryMap.erase(key); }
	bool hasKey(long key) { return memoryMap.count(key); }

private:
	std::unordered_map<long, memoryBase*> memoryMap;
};
//}}}
//}}}
#endif

// Function prototypes {{{
void newMemoryItem(struct yCudaCommandStruct* command);
void deleteMemoryItem(struct yCudaCommandStruct* command);
void setMemoryItem(struct yCudaCommandStruct* command);
void getMemoryItem(struct yCudaCommandStruct* command);
void queryMemoryItem(struct yCudaCommandStruct* command);
void purgeMemory();
void compileModule(struct yCudaCommandStruct* command);
void loadModule(char* modulePath);
void unloadModule(char* modulePath);
void loadFunction(char* funcName, char* modulePath);
void unloadFunction(char* funcName);
void launchKernel(struct yCudaCommandStruct* command);
//}}}

#endif /* MEMORY_HPP */
