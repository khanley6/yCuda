#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>        // std::string
//#include <unistd.h>    // Unused
#include <unordered_map> // std::unordered_map
#include <cuda.h>        // CUfunction, CUmodule,
                         // cuModuleGetFunction(), CUDA_SUCCESS

class cudaFunctions
{
private:
	std::unordered_map<std::string, CUfunction*> functionMap;

public:
	//cudaFunctions();
	~cudaFunctions();

	void loadFunction(std::string functionName, CUmodule* module);
	void unloadFunction(std::string functionName);
	CUfunction* getFunction(std::string functionName);
};


#endif /* FUNCTIONS_H */
