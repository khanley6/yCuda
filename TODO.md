# Todo

## Urgent
* Check safety
  * What happens when cuArrayGet is called on invalid key?
* cuArraySet is seriously broken!
* Daemons are lauched every time yCuda.i is included - I think?


## Bugs
* Issue with pathname for modules and functions - daemon dies on incorrect name


## Short term
* make check
  * deviceProp
  * Should be easily done with a simple C++ program
* Implement more functionality
  * Implement runtime compilation of compute kernels
* Add more test/example kernels
* make check
  * deviceProp
* Implement all Yorick data types for all algorithms


## Long term
* Implement a managed shared memory space to reduce unnecessary data copies
* Add support for Cuda streams
* Support relative path names to .cu/.ptx files
* Command queue for Async-ish work


## Short term research
* Loading of libraries - cuFFT, cuBLAS, etc.
* Changed shared memory for command communication for message queue?
* Randomize shared memory file names to allow multiple instances to run


## Long term research
* Shared memory should be protected with mutexes?
* Async at some point?
