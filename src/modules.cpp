#include "modules.hpp"
#include "memory.hpp"

void cudaModules::modulePathToName(std::string& modulePath) {
	modulePath.erase(0, modulePath.find_last_of("/")+1);
	modulePath.erase(modulePath.find_last_of("."), modulePath.length());
}

std::string cudaModules::cuToPtx(const std::string& modulePath) {
	std::string ptxPath(modulePath);
	ptxPath.erase(ptxPath.find_last_of("."), ptxPath.length());
	ptxPath += ".ptx";

	return ptxPath;
}

cudaModules::~cudaModules() {
	for (auto it : moduleMap) {
		delete it.second;
	}

	moduleMap.clear();
}

void cudaModules::loadModule(std::string modulePath) {
	CUmodule* newModule = new CUmodule;
	
	if (modulePath.find(".cu")) {
		std::string ptxPath = cuToPtx(modulePath);

		struct stat buffer;
		if (stat(ptxPath.c_str(), &buffer) != 0) {
			std::string systemCommand = "nvcc -ptx " + modulePath + " -o "
				+ ptxPath + " > /dev/null";

			system(systemCommand.c_str());
		}

		modulePath = ptxPath;
	}

	if (cuModuleLoad(newModule, modulePath.c_str()) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	modulePathToName(modulePath);

	moduleMap.insert(std::make_pair(modulePath, newModule));
}

void cudaModules::unloadModule(std::string modulePath) {
	modulePathToName(modulePath);

	if (cuModuleUnload(*moduleMap[modulePath]) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	delete moduleMap[modulePath];
	moduleMap.erase(modulePath);
}

CUmodule* cudaModules::getModule(std::string modulePath) {
	modulePathToName(modulePath);
	return moduleMap[modulePath];
}
