#include <iostream>    // std::cout
//#include <sstream>     //
//#include <cstdlib>     //
#include <sys/types.h> // pid_t
//#include <sys/stat.h>  // 
#include <sys/wait.h>  // wait
#include <sys/mman.h>  // mmap, munmap, shm_open, shm_unlink,
                       // PROT_READ, PROT_WRITE, MAP_SHARED, MAP_FAILED
#include <fcntl.h>     // O_RDWR, O_CREAT
#include <errno.h>     // EXIT_FAILURE
//#include <unistd.h>    //
#include <signal.h>    // kill

#include <cuda.h>      // All the CUDA definitions/functions
//#include <cuda_runtime.h> //

#include "common.hpp"
#include "memory.hpp"
#include "modules.hpp"
#include "functions.hpp"
#include "daemon.hpp"

volatile int isSyncCommand = 0;

struct shmHeap sharedHeap;
struct shmCmd  sharedCommand;
volatile struct shmStat sharedStatus;

cudaModules modules;
cudaFunctions functions;
memoryItemMap memory;

// NOFORK DEBUG {{{
static void printCommand(struct yCudaCommandStruct* shCmd) {
    std::cout << ">>> Start shared memory block <<<" << "\n";
	// cType {{{
	std::cout << "cType = ";
	switch (shCmd->cType) {
		case MEM_NEW:
			std::cout << "MEM_NEW\n";
			break;
		case MEM_DELETE:
			std::cout << "MEM_DELETE\n";
			break;
		case MEM_GET:
			std::cout << "MEM_GET\n";
			break;
		case MEM_SET:
			std::cout << "MEM_SET\n";
			break;
		case MEM_QUERY:
			std::cout << "MEM_QUERY\n";
			break;
		case MEM_PURGE:
			std::cout << "MEM_PURGE\n";
			break;
		case MOD_COMPILE:
			std::cout << "MOD_COMPILE\n";
			break;
		case MOD_LOAD:
			std::cout << "MOD_LOAD\n";
			break;
		case MOD_UNLOAD:
			std::cout << "MOD_UNLOAD\n";
			break;
		case FUNC_LOAD:
			std::cout << "FUNC_LOAD\n";
			break;
		case FUNC_UNLOAD:
			std::cout << "FUNC_UNLOAD\n";
			break;
		case KERN_LAUNCH:
			std::cout << "KERN_LAUNCH\n";
			break;
		default:
			std::cout << "OTHER...\n";
	}
	//}}}
	// dType {{{
	std::cout << "dType = ";
	switch(shCmd->dType) {
		case CHAR:
			std::cout << "CHAR\n";
			break;
		case SHORT:
			std::cout << "SHORT\n";
			break;
		case INT:
			std::cout << "INT\n";
			break;
		case LONG:
			std::cout << "LONG\n";
			break;
		case FLOAT:
			std::cout << "FLOAT\n";
			break;
		case DOUBLE:
			std::cout << "DOUBLE\n";
			break;
		default:
			std::cout << "OTHER...\n";
	}
	//}}}
	// memoryKey {{{	
	std::cout << "memoryKey = " << shCmd->memoryKey << "\n";
	//}}}
	// dims {{{	
	std::cout << "dims = {" << shCmd->dims[0] << ","
													<< shCmd->dims[1] << ","
													<< shCmd->dims[2] << ","
													<< shCmd->dims[3] << ","
													<< shCmd->dims[4] << ","
													<< shCmd->dims[5] << ","
													<< shCmd->dims[6] << ","
													<< shCmd->dims[7] << ","
													<< shCmd->dims[8] << ","
													<< shCmd->dims[9] << ","
													<< shCmd->dims[10]
													<< "}\n";
	//}}}
	// numElements {{{	
	std::cout << "numElements = " << shCmd->numElements << "\n";
	//}}}
	// dataSize {{{	
	std::cout << "dataSize = " << shCmd->dataSize << "\n";
	//}}}
	// modulePath {{{	
	std::cout << "modulePath = " << shCmd->modulePath << "\n";
	//}}}
	// funcName {{{	
	std::cout << "funcName = " << shCmd->funcName << "\n";
	//}}}
	// dataPtr {{{	
	std::cout << "*dataPtr = {";
	switch(shCmd->dType) {
		case CHAR:
			std::cout << ((char*)sharedHeap.heap)[0] << ",";
			std::cout << ((char*)sharedHeap.heap)[1] << ",";
			std::cout << ((char*)sharedHeap.heap)[2] << ",";
			break;
		case SHORT:
			std::cout << ((short*)sharedHeap.heap)[0] << ",";
			std::cout << ((short*)sharedHeap.heap)[1] << ",";
			std::cout << ((short*)sharedHeap.heap)[2] << ",";
			break;
		case INT:
			std::cout << ((int*)sharedHeap.heap)[0] << ",";
			std::cout << ((int*)sharedHeap.heap)[1] << ",";
			std::cout << ((int*)sharedHeap.heap)[2] << ",";
			break;
		case LONG:
			std::cout << ((long*)sharedHeap.heap)[0] << ",";
			std::cout << ((long*)sharedHeap.heap)[1] << ",";
			std::cout << ((long*)sharedHeap.heap)[2] << ",";
			break;
		case FLOAT:
			std::cout << ((float*)sharedHeap.heap)[0] << ",";
			std::cout << ((float*)sharedHeap.heap)[1] << ",";
			std::cout << ((float*)sharedHeap.heap)[2] << ",";
			break;
		case DOUBLE:
			std::cout << ((double*)sharedHeap.heap)[0] << ",";
			std::cout << ((double*)sharedHeap.heap)[1] << ",";
			std::cout << ((double*)sharedHeap.heap)[2] << ",";
			break;
		default:
			std::cout << "OTHER,";
	}
	std::cout << "...}\n";
	//}}}
    std::cout << ">>> End shared memory block <<<" << "\n\n";
}
//}}}

int main(int argc, char *argv[])
{
	// Temporary signal blocking {{{
	sigset_t mask;
	sigset_t orig_mask;
	struct sigaction act;

	std::memset(&act, 0, sizeof(act));

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);

	if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}

#ifndef NOFORK
	// Forking {{{
	int status;
	pid_t pid, sid;

	// Fork 1 {{{
	if ((pid = fork()) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		// Parent process
		wait(&status);
		return 0;
	}
	//}}}
	// Fork 2 {{{
	if ((pid = fork()) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		// Parent process
		int pid_fd = shm_open(shmPIDFile, O_RDWR | O_CREAT, 0600);
		
		ftruncate(pid_fd, sizeof(pid_t));

		pid_t *pid_sh = (pid_t*)mmap(NULL, sizeof(pid_t), PROT_READ|PROT_WRITE,
				MAP_SHARED, pid_fd, 0);

		*pid_sh = pid;

		close(pid_fd);
		munmap(pid_sh, sizeof(pid_t));
	
		exit(EXIT_SUCCESS);
	}
	//}}}

	umask(0);

	if ((sid = setsid()) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	if ((chdir("/")) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	//}}}
#else
	// No forking - Just pass PID to parent {{{
	int pid_fd = shm_open(shmPIDFile, O_RDWR | O_CREAT, 0600);

	ftruncate(pid_fd, sizeof(pid_t));

	pid_t *pid_sh = (pid_t*)mmap(NULL, sizeof(pid_t), PROT_READ|PROT_WRITE,
			MAP_SHARED, pid_fd, 0);

	*pid_sh = ::getpid();

	close(pid_fd);
	munmap(pid_sh, sizeof(pid_t));
	//}}}
#endif

	// atexit() {{{
	if (std::atexit(shmHeapCleanup) != 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	
	if (std::atexit(shmCommandCleanup) != 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	
	if (std::atexit(shmStatusCleanup) != 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}

	// Shared Memory {{{
	// Shared heap {{{
	sharedHeap.size = 1<<12;
	sharedHeap.minSize = sharedHeap.size;

	sharedHeap.fd = shm_open(shmHeapFile, O_RDWR|O_CREAT, 0600);
	if (sharedHeap.fd == -1) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	ftruncate(sharedHeap.fd, sharedHeap.size);

	sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
			MAP_SHARED, sharedHeap.fd, 0);
	if (sharedHeap.heap == MAP_FAILED) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}
	// Shared command {{{
	sharedCommand.size = sizeof(struct yCudaCommandStruct);

	sharedCommand.fd = shm_open(shmCommandFile, O_RDWR|O_CREAT, 0600);
	if (sharedCommand.fd == -1) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	ftruncate(sharedCommand.fd, sharedCommand.size);

	sharedCommand.sharedCommandStruct = (struct yCudaCommandStruct*)mmap(
			NULL, sharedCommand.size, PROT_READ|PROT_WRITE,
			MAP_SHARED, sharedCommand.fd, 0);
	if (sharedCommand.sharedCommandStruct == MAP_FAILED) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}
	// Shared daemon status {{{
	sharedStatus.size = sizeof(daemonStatus);

	sharedStatus.fd = shm_open(shmStatusFile, O_RDWR|O_CREAT, 0600);
	if (sharedStatus.fd == -1) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	ftruncate(sharedStatus.fd, sharedStatus.size);

	sharedStatus.status = (daemonStatus*)mmap(NULL, sharedStatus.size,
			PROT_READ|PROT_WRITE, MAP_SHARED, sharedStatus.fd, 0);
	if (sharedStatus.status == MAP_FAILED) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}
	//}}}
	
	// Cuda setup {{{
	// TODO: Potentiall create multiple devices?
	CUdevice cuDevice;
	CUcontext cuContext;

	cuInit(0);
	cuDeviceGet(&cuDevice, 0);
	cuCtxCreate(&cuContext, 0, cuDevice);
	//}}}
	
	// Signal handling {{{
	struct sigaction sa;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = handler;

	if(sigaction(SIGUSR1, &sa, NULL) == -1) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	if(sigaction(SIGUSR2, &sa, NULL) == -1) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	if (sigprocmask(SIG_SETMASK, &orig_mask, NULL) < 0) {
		*sharedStatus.status = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
	//}}}
	
	// Main loop {{{
	while (true) {
		*sharedStatus.status = DAEMON_WAITING;
		sigsuspend(&sa.sa_mask);

		if (isSyncCommand) {
			cuCtxSynchronize();
			isSyncCommand = 0;
			continue;
		}

#ifdef NOFORK
        if (sharedCommand.sharedCommandStruct->dataSize > sharedHeap.size) {
            munmap(sharedHeap.heap, sharedHeap.size);
            sharedHeap.size = sharedCommand.sharedCommandStruct->dataSize;
            sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
                    MAP_SHARED, sharedHeap.fd, 0);
        }
		printCommand(sharedCommand.sharedCommandStruct);
#endif

		interpretSharedCommand(sharedCommand.sharedCommandStruct);
	}
	//}}}
	
	// Exit cleanup {{{
	cuCtxDestroy(cuContext);
	//}}}
	
	return 0;
}

// Interpret shared memory {{{
void interpretSharedCommand(struct yCudaCommandStruct* command) {
	if (command->dataSize > sharedHeap.size) {
		munmap(sharedHeap.heap, sharedHeap.size);
		sharedHeap.size = command->dataSize;
		sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedHeap.fd, 0);
	}

	switch (command->cType) {
		case MEM_NEW:
			newMemoryItem(command);
			break;
		case MEM_DELETE:
			deleteMemoryItem(command);
			break;
		case MEM_SET:
			setMemoryItem(command);
			break;
		case MEM_GET:
			getMemoryItem(command);
			break;
		case MEM_QUERY:
			queryMemoryItem(command);
			break;
		case MEM_PURGE:
			purgeMemory();
			break;
		case MOD_COMPILE:
			compileModule(command);
			break;
		case MOD_LOAD:
			modules.loadModule(std::string(command->modulePath));
			break;
		case MOD_UNLOAD:
			modules.unloadModule(std::string(command->modulePath));
			break;
		case FUNC_LOAD:
			functions.loadFunction(std::string(command->funcName),
					modules.getModule(std::string(command->modulePath)));
			break;
		case FUNC_UNLOAD:
			functions.unloadFunction(std::string(command->funcName));
			break;
		case KERN_LAUNCH:
			launchKernel(command);
			break;
	}
}
//}}}

// Signal handler {{{
static void handler(int sig) {
	*sharedStatus.status = DAEMON_BUSY;
	switch (sig) {
		case SIGUSR1:
			break;
		case SIGUSR2:
			isSyncCommand = 1;
			break;
	}
}
//}}}

// Daemon cleanup {{{
static void shmHeapCleanup() {
	munmap(sharedHeap.heap, sharedHeap.size);
	close(sharedHeap.fd);
	shm_unlink(shmHeapFile);
}

static void shmCommandCleanup() {
	munmap(sharedCommand.sharedCommandStruct, sharedCommand.size);
	close(sharedCommand.fd);
	shm_unlink(shmCommandFile);
}

static void shmStatusCleanup() {
	munmap(sharedStatus.status, sharedStatus.size);
	close(sharedStatus.fd);
	shm_unlink(shmStatusFile);
}
//}}}
