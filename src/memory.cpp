#include "memory.hpp"

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

// From yapi.h
#define Y_CHAR 0
#define Y_SHORT 1
#define Y_INT 2
#define Y_LONG 3
#define Y_FLOAT 4
#define Y_DOUBLE 5
#define Y_COMPLEX 6
#define Y_STRING 7
#define Y_POINTER 8
#define Y_STRUCT 9

// memoryItem {{{
template <typename T>
memoryItem<T>::memoryItem(T* inputData) :
	memoryBase(const_cast<std::type_info*>(&typeid(inputData)))
{
	data = nullptr;
	deviceData = 0;
}

template <typename T>
memoryItem<T>::memoryItem(T* inputData, size_t* dims, size_t numElements) :
	memoryBase(const_cast<std::type_info*>(&typeid(inputData)))
{
	data = new T[numElements];
	std::memcpy(dataDimensions, dims, Y_DIMSIZE*sizeof(size_t));
	std::memcpy(data, inputData, numElements*sizeof(T));

	sizeInElements = numElements;
	sizeInBytes = numElements*sizeof(T);

	// Allocate memory on device
	if (cuMemAlloc(&deviceData, sizeInBytes) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	if (cuMemcpyHtoD(deviceData, data, sizeInBytes) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
}

template <typename T>
memoryItem<T>::~memoryItem() {
	if (deviceData)
		cuMemFree(deviceData);

	if (data)
		delete[] data;
};

template <typename T>
void memoryItem<T>::setData(void* input, size_t* dims, size_t inputSizeInBytes) {
	if (sizeInBytes != inputSizeInBytes) {
		delete data;
		sizeInElements = inputSizeInBytes/sizeof(T);
		data = new T[sizeInElements];
		sizeInBytes = inputSizeInBytes;

		std::memcpy(dataDimensions, dims, Y_DIMSIZE*sizeof(size_t));

		// device data
		if (deviceData)
			cuMemFree(deviceData);

		if (cuMemAlloc(&deviceData, sizeInBytes) != CUDA_SUCCESS) {
			*(sharedStatus.status) = DAEMON_ERROR;
			exit(EXIT_FAILURE);
		}
	}

	std::memcpy(data, input, sizeInBytes);

	if (cuMemcpyHtoD(deviceData, data, sizeInBytes) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}
}

template <typename T>
void* memoryItem<T>::getData() {
	if (cuMemcpyDtoH(data, deviceData, sizeInBytes) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	return (void*)data;
}
//}}}

// memoryItemMap {{{
memoryItemMap::~memoryItemMap() {
	for (auto it = memoryMap.cbegin(); it != memoryMap.cend(); ++it) {
		delete it->second;
	}

	memoryMap.clear();
}

void memoryItemMap::newItem(long key, dataType type) {
	switch (type) {
		case CHAR:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<char>(0)));
			break;
		case SHORT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<short>(0)));
			break;
		case INT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<int>(0)));
			break;
		case LONG:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<long>(0)));
			break;
		case FLOAT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<float>(0)));
			break;
		case DOUBLE:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<double>(0)));
			break;
		default:
			break; // TODO: Implement other data types
	}
}

void memoryItemMap::newItem(long key, dataType type, void* dataPtr, size_t* dims, size_t numElements) {
	switch (type) {
		case CHAR:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<char>((char*)dataPtr, dims, numElements)));
			break;
		case SHORT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<short>((short*)dataPtr, dims, numElements)));
			break;
		case INT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<int>((int*)dataPtr, dims, numElements)));
			break;
		case LONG:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<long>((long*)dataPtr, dims, numElements)));
			break;
		case FLOAT:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<float>((float*)dataPtr, dims, numElements)));
			break;
		case DOUBLE:
			memoryMap.insert(std::make_pair(key,
						new memoryItem<double>((double*)dataPtr, dims, numElements)));
			break;
		default:
			break; // TODO: Implement other data types
	}
}

void memoryItemMap::setItem(long key, dataType type, void* dataPtr, size_t* dims, size_t numElements) {
	size_t totalSize = 0;

	switch (type) {
		case CHAR:
			totalSize = numElements*sizeof(char);
			break;
		case SHORT:
			totalSize = numElements*sizeof(char);
			break;
		case INT:
			totalSize = numElements*sizeof(char);
			break;
		case LONG:
			totalSize = numElements*sizeof(char);
			break;
		case FLOAT:
			totalSize = numElements*sizeof(char);
			break;
		case DOUBLE:
			totalSize = numElements*sizeof(char);
			break;
		default:
			break;
	}

	memoryMap[key]->setData(dataPtr, dims, totalSize);
}

void memoryItemMap::getItem(long key, dataType* type, void* destPtr, size_t* dims, size_t* numElements, size_t* dataSize) {
	void* dataToReturn      = memoryMap[key]->getData();
	size_t numBytesToReturn = memoryMap[key]->getSize();
	size_t* dimsToReturn    = memoryMap[key]->getDims();

	// TODO: Check size of sharedHeap.heap. Ensure enough space is
	//       available to store data
	//       This will require a client-side check also
	//       Is this possible?
	if (numBytesToReturn > sharedHeap.size) {
		// if so, unmap the shared memory, resize and remap
		munmap(sharedHeap.heap, sharedHeap.size);
		sharedHeap.size = numBytesToReturn;
		ftruncate(sharedHeap.fd, sharedHeap.size);
		sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedHeap.fd, 0);
	}

	std::memcpy(destPtr, dataToReturn, numBytesToReturn);

	std::memcpy(dims, dimsToReturn, Y_DIMSIZE*sizeof(size_t));

	*numElements = memoryMap[key]->getElems();
	*dataSize = numBytesToReturn;

	// FIXME: Replace with switch
	if (*memoryMap[key]->dataType == typeid(char*)) {
		*type = CHAR;
	} else if (*memoryMap[key]->dataType == typeid(short*)) {
		*type = SHORT;
	} else if (*memoryMap[key]->dataType == typeid(int*)) {
		*type = INT;
	} else if (*memoryMap[key]->dataType == typeid(long*)) {
		*type = LONG;
	} else if (*memoryMap[key]->dataType == typeid(float*)) {
		*type = FLOAT;
	} else if (*memoryMap[key]->dataType == typeid(double*)) {
		*type = DOUBLE;
	} else {
		*type = VOID;
	}
}

void memoryItemMap::queryItem(long key, dataType* type, size_t* dims, size_t* numElements) {
	size_t* dimsToReturn    = memoryMap[key]->getDims();
	std::memcpy(dims, dimsToReturn, Y_DIMSIZE*sizeof(size_t));

	*numElements = memoryMap[key]->getElems();
	
	// FIXME: Replace with switch
	if (*memoryMap[key]->dataType == typeid(char*)) {
		*type = CHAR;
	} else if (*memoryMap[key]->dataType == typeid(short*)) {
		*type = SHORT;
	} else if (*memoryMap[key]->dataType == typeid(int*)) {
		*type = INT;
	} else if (*memoryMap[key]->dataType == typeid(long*)) {
		*type = LONG;
	} else if (*memoryMap[key]->dataType == typeid(float*)) {
		*type = FLOAT;
	} else if (*memoryMap[key]->dataType == typeid(double*)) {
		*type = DOUBLE;
	} else {
		*type = VOID;
	}
}

void memoryItemMap::purgeItems() {
	for (auto it = memoryMap.cbegin(); it != memoryMap.cend(); ++it) {
		delete it->second;
	}

	memoryMap.clear();
}
//}}}

// memoryItemMap interaction {{{
extern memoryItemMap memory;
extern cudaFunctions functions;

void newMemoryItem(struct yCudaCommandStruct* command) {
	if (memory.hasKey(command->memoryKey)) {
		memory.setItem(command->memoryKey,
				command->dType,
				sharedHeap.heap,
				command->dims,
				command->numElements);
	} else {
		if (command->dataPtr == NULL) {
			memory.newItem(command->memoryKey,
					command->dType);
		} else {
			memory.newItem(command->memoryKey,
					command->dType,
					sharedHeap.heap,
					command->dims,
					command->numElements);
		}
	}
}

void deleteMemoryItem(struct yCudaCommandStruct* command) {
	if (memory.hasKey(command->memoryKey)) {
		memory.deleteItem(command->memoryKey);
	}
}

void setMemoryItem(struct yCudaCommandStruct* command) {
	if (memory.hasKey(command->memoryKey)) {
		memory.setItem(command->memoryKey,
				command->dType,
				sharedHeap.heap,
				command->dims,
				command->numElements);
	} else {
		memory.setItem(command->memoryKey,
				command->dType,
				sharedHeap.heap,
				command->dims,
				command->numElements);
	}
}

void getMemoryItem(struct yCudaCommandStruct* command) {
	if (memory.hasKey(command->memoryKey)) {
		memory.getItem(command->memoryKey,
				&command->dType,
				sharedHeap.heap,
				command->dims,
				&command->numElements,
				&command->dataSize);
	}
}

void queryMemoryItem(struct yCudaCommandStruct* command) {
	if (memory.hasKey(command->memoryKey)) {
		memory.queryItem(command->memoryKey,
				&command->dType,
				command->dims,
				&command->numElements);
	}
}

void purgeMemory() {
	memory.purgeItems();
}

void compileModule(struct yCudaCommandStruct* command) {
	// call 'nvcc -ptx $INPUT_PATH -o $OUTPUT_PATH'
	// ONLY SUPPORTS ABSOLUTE PATHS
	std::string inputPath(command->modulePath);
	std::string outputPath(command->modulePath);

	outputPath.erase(outputPath.find_last_of("."),
			outputPath.length());

	std::string systemCommand = "nvcc -ptx " + inputPath + " -o " +
		outputPath + ".ptx > /dev/null";

	system(systemCommand.c_str());
}

void launchKernel(struct yCudaCommandStruct* command) {

	void** args = NULL;
	dataUnion* scalars = NULL;

	if (command->numElements > 1) {
		args = new void*[(command->numElements)/2]; // discard encoding info

		scalars = new dataUnion[(command->numElements)/2];

		long* keys = (long*)sharedHeap.heap;

		for (int i = 0, j = 0; i < command->numElements; i += 2, j++) {
			if (keys[i] > 0) { // memory key
				args[j] = memory.getDevicePtr(keys[i]);
			} else { // scalar value
				scalars[j].l = keys[i+1];
				switch (keys[i]) {
					case -(1<<Y_CHAR):
						args[j] = &scalars[j].c;
						break;
					case -(1<<Y_SHORT):
						args[j] = &scalars[j].s;
						break;
					case -(1<<Y_INT):
						args[j] = &scalars[j].i;
						break;
					case -(1<<Y_LONG):
						args[j] = &scalars[j].l;
						break;
					case -(1<<Y_FLOAT):
						args[j] = &scalars[j].f;
						break;
					case -(1<<Y_DOUBLE):
						args[j] = &scalars[j].d;
						break;
					default:
						// Shouldn't get here
						break;
				}
			}
		}
	}

	// Why..?
	//CUfunction* func = functions.getFunction(std::string(command->funcName));

	cuLaunchKernel(
			*functions.getFunction(std::string(command->funcName)),
			(unsigned int)(command->dims[0]),  // gridDimX
			(unsigned int)(command->dims[1]),  // gridDimY
			(unsigned int)(command->dims[2]),  // gridDimZ
			(unsigned int)(command->dims[3]),  // blockDimX
			(unsigned int)(command->dims[4]),  // blockDimY
			(unsigned int)(command->dims[5]),  // blockDimZ
			0,                 // sharedMemBytes
			NULL,              // CUstream
			args,              // arguments
			NULL);             // extras??

	cuCtxSynchronize();

	if (command->numElements > 1) {
		delete[] args;
		delete[] scalars;
	}
}
//}}}
