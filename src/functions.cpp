#include "functions.hpp"
#include "memory.hpp"

cudaFunctions::~cudaFunctions() {
	for (auto it : functionMap) {
		delete it.second;
	}

	functionMap.clear();
}

void cudaFunctions::loadFunction(std::string funcName, CUmodule* module) {
	CUfunction* newFunction = new CUfunction;
	if (cuModuleGetFunction(newFunction, *module, funcName.c_str()) != CUDA_SUCCESS) {
		*(sharedStatus.status) = DAEMON_ERROR;
		exit(EXIT_FAILURE);
	}

	functionMap.insert(std::make_pair(funcName, newFunction));
}

void cudaFunctions::unloadFunction(std::string functionName) {
	delete functionMap[functionName];
	functionMap.erase(functionName);
}

CUfunction* cudaFunctions::getFunction(std::string functionName) {
	return functionMap[functionName];
}
