/*
 * yCuda.i
 *
 * A Cuda interface for the Yorick programming language
 *
 * -----------------------------------------------------------------------------
 *
 * Choose a license
 *
 *------------------------------------------------------------------------------
 *
 *
 *
 *------------------------------------------------------------------------------
 * todo:
 *  Everything really
 *
 * log:
 *
 *------------------------------------------------------------------------------
 *
 */

// Environment Variables

if (is_func(plug_in)) plug_in, "yCuda";

YCUDA_DAEMON_PID = -1;

// Daemon Interaction {{{
extern yCudaInit;
extern yCudaKill;
extern yCudaSync;
extern yCudaPIDs;

func cuInit(nil)
/* DOCUMENT cuInit()
 *
 * Returns the PID of the yCuda daemon
 *
 */
{
    extern YCUDA_DAEMON_PID;

    if (YCUDA_DAEMON_PID < 0)
        YCUDA_DAEMON_PID = yCudaInit(Y_HOME+"lib/yCudaDaemon");

    if (YCUDA_DAEMON_PID < 0)
        return -1;
    return YCUDA_DAEMON_PID;
}

func cuKill(nil)
/* DOCUMENT cuKill()
 *
 * Kills the yCuda daemon if it's running
 *
 */
{
    extern YCUDA_DAEMON_PID;
    extern memList;
    extern memListIndex;
    extern memListCount;

    memListKeys(:) = 0;
    memList(:) = memoryItem(0);

    memListIndex = 1;
    memListCount = 0;

    if (YCUDA_DAEMON_PID > 1) {
        yCudaKill(YCUDA_DAEMON_PID);
        YCUDA_DAEMON_PID = -1;
    }
}

func cuSync(nil)
/* DOCUMENT cuSync()
 *
 * Sends SIGUSR2 to thd yCuda daemon to force synchronization
 *
 */
{
    if (YCUDA_DAEMON_PID > 1) {
        yCudaSync(YCUDA_DAEMON_PID);
    }
}

func cuRunning(nil)
/* DOCUMENT cuRunning()
 *
 * Runs `ps -ef | grep [y]CudaDaemon` to get PIDs of all running instances
 * of yCudaDaemon. The PIDs are returned to Yorick and compared to the value
 * of YCUDA_DAEMON_PID
 *
 * The backend function Y_yCudaPIDs assumes no more than 64 instances of
 * yCudaDaemon running
 *
 */
{
    PIDs = yCudaPIDs();
    return (anyof(PIDs == YCUDA_DAEMON_PID));
}

func cuPID(nil)
/* DOCUMENT cuPID()
 *
 * Returns pid of yCuda daemon
 *
 */
{
    return YCUDA_DAEMON_PID;
}
//}}}
// Utilities {{{
extern yCudaGetVersion;
extern encodeToLong;
extern decodeFromLong;

func cuGetVersion(nil)
/* DOCUMENT yCuda_version()
 *
 * Returns the version string of the Yorick interface to the yCuda library
 *
 */
{
    //TODO
    return 0.01;
}

func cuEncodeToLong(val)
/* DOCUMENT encodeToLong(val)
 *
 * Bitwise casts val to long
 *
 */
{
    return encodeToLong(val);
}

func cuDecodeFromLong(val, type)
/* DOCUMENT decodeFromLong(val, type)
 *
 * Bitwise casts long to type (Y_LONG, Y_DOUBLE, etc.)
 *
 */
{
    // decodeFromLong returns array so return first element
    return decodeFromLong(val, type)(1);
}

func typeofID(val)
/* DOCUMENT typeofID(val)
 * Yorick function typeof() returns a string describing the type of value.
 * This function returns the numerical ID in terms of Y_LONG, Y_DOUBLE, etc.
 */
{
    strID = typeof(val);
    retID = 0;

    if (strID == "char")
        retID = Y_CHAR;
    else if (strID == "short")
        retID = Y_SHORT;
    else if (strID == "int")
        retID = Y_INT;
    else if (strID == "long")
        retID = Y_LONG;
    else if (strID == "float")
        retID = Y_FLOAT;
    else if (strID == "double")
        retID = Y_DOUBLE;
    else
        error("Unsupported data");

    return retID;
}

func idToTypeStr(val)
/* DOCUMENT idToTypeStr(val)
 * Converts result of typeofID() back to string
 */
{
    retID = "";

    if (val == Y_CHAR)
        retID = "char";
    else if (val == Y_SHORT)
        retID = "short";
    else if (val == Y_INT)
        retID = "int";
    else if (val == Y_LONG)
        retID = "long";
    else if (val == Y_FLOAT)
        retID = "float";
    else if (val == Y_DOUBLE)
        retID = "double";
    else
        error("Unsupported data");

    return retID;
}

func cuArgs(arg, ..) 
/* DOCUMENT cuArgs(arg, ..)
 * cuArgs encodes a variadic list of arguments of size N to an array of size
 * 2*N. This array includes information of argument types, whether they are
 * memoryKeys to retrieve from the daemon or scalars. In the case of a scalar
 * value, the second piece of information encodes the type
 *
 * e.g. cuArgs(&a) returns 
 *            [0xFF, 0xFF] 
 *            where 0xFF is an example memory key
 *
 *      cuArgs(1.2) returns
 *            [-(1<<Y_DOUBLE), cuEncodeToLong(1.2)]
 *            The first value is negative to avoid conflict with memory keys
 *            in the off chance (1<<Y_TYPE)==cuEncodeToLong(arg)
 */
{
    tmpArg = arg;
    numArgs = more_args();

    if (tmpArg) {
        args = array(long, (numArgs+1)*2);

        arrayIdx = 1;

        // Execute loop on arg and additional args
        do {
            if (typeof(tmpArg) == "pointer") {
                args(arrayIdx) = args(arrayIdx+1) = cuEncodeToLong(tmpArg);
            } else {
                args(arrayIdx) = -(1<<(typeofID(tmpArg)));
                args(arrayIdx+1) = cuEncodeToLong(tmpArg);
            }

            arrayIdx += 2;
            tmpArg = next_arg();
        } while(numArgs--);
    } else {
        args = [0];
    }

    return args;
}
//}}}
// memoryItem database {{{
struct memoryItem {
    long memoryKey;
    long dims(11);
    long type;
    long size;
};


// 256 memory items initially - Allowed to grow if needed
memListIndex = 1;
memListCount = 0;
memListCapacity = 256;
memList = array(memoryItem, memListCapacity);

// Store keys separately also, this allows quick traversal to check
// if key is present
memListKeys = array(long, memListCapacity);

// memoryItem database utilities {{{
func cuVariableInfo(&var)
/* DOCUMENT cuVariableInfo(&var)
 *
 * Checks client for presence of var and returns its metadata
 *
 */
{
    idx = where(memListKeys==cuEncodeToLong(var));

    if (numberof(idx)==1) {
        idx = idx(1);
        dims = memList(idx).dims;

        infoStr  = swrite(format="memoryKey = %i\n", memList(idx).memoryKey);
        infoStr += swrite(format=" dims = [%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i]\n",
                dims(1),dims(2),dims(3),dims(4),dims(5),dims(6),dims(7),dims(8),dims(9),dims(10),dims(11));
        infoStr += swrite(format=" type = %i,(%s)\n", memList(idx).type, idToTypeStr(memList(idx).type));
        infoStr += swrite(format=" size = %i", memList(idx).size);

        write, infoStr;
    } else {
        write, swrite(format="No variable with memoryKey %i found", cuEncodeToLong(var));
    }
}

func cuDeviceInfo(nil)
/* DOCUMENT cuDeviceInfo()
 *
 * Prints total memory used by yCuda variables
 * TODO: Can be extended to return a lot more info
 *
 */
{
    totalMemory = 0;
    for (i=1; i<memListIndex; ++i) {
        if (memList(i).size > 0) {
            totalMemory += memList(i).size;
        }
    }

    write, swrite(format="Total memory usage: %i bytes by %i variables", totalMemory, memListCount);
}

//}}}
// Garbage collector {{{
func memGarbageCollector(nil) {
    extern memList;
    extern memListKeys;
    extern memListIndex;
    extern memListCount;
    extern memListCapacity;

    tempList = memList;
    tempListKeys = memListKeys;
    memList(:) = memoryItem(0);
    memListKeys(:) = 0;

    for (i=1, j=1; i<=memListCapacity; ++i) {
        if (tempList(i).memoryKey > 0) {
            memListKeys(j) = tempListKeys(i);
            memList(j++) = tempList(i);
        }
    }

    memListIndex = j;

    if (memListCount/(float(memListCapacity)) > 0.9) {
        grow, memList, array(memoryItem, 128);
        memListCapacity = dimsof(memList)(2);

        grow, memListKeys, array(long, 128);
    }
}
//}}}
//}}}

// DEPRECATED: Create Array {{{
func cuArrayDEP(&input)
/* DOCUMENT cuArrayDEP(&array)
 *
 * DEPRECATED
 *
 * Makes array available to cuda device using the yorick pointer
 * value as the memory key
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArray expectes pointer arguments");

    // Check if variable already exists
    if (!numberof(where(memListKeys == cuEncodeToLong(input)))) {

        // Allocate array with daemon and store memory key on client
        memList(memListIndex).memoryKey = yCudaArray(YCUDA_DAEMON_PID, input, *input);
        memList(memListIndex).dims(1:dimsof(*input)(1)+1) = dimsof(*input);
        memList(memListIndex).type = typeofID(*input);
        memList(memListIndex).size = sizeof(*input);

        // Update local key list
        memListKeys(memListIndex) = memList(memListIndex).memoryKey;

        // Update index
        memListIndex++;
        memListCount++;
    }
}
//}}}
// Create Array {{{
extern yCudaArray;
func cuArray(&input, ..)
/* DOCUMENT cuArray(&input, ..)
 *
 * Makes a variadic list of arrays available to cuda device using the yorick 
 * pointer value as the memory key
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    arg = input;
    numArgs = more_args();
    argCounter = 1;

    do {
        if (typeof(arg) != "pointer") {
            nil = write(format="Skippng argument %i as it is not a pointer\n", argCounter);
        } else {
            // Check if variable already exists
            if (!numberof(where(memListKeys == cuEncodeToLong(arg)))) {

                // Allocate array with daemon and store memory key on client
                memList(memListIndex).memoryKey = yCudaArray(YCUDA_DAEMON_PID, arg, *arg);
                memList(memListIndex).dims(1:dimsof(*arg)(1)+1) = dimsof(*arg);
                memList(memListIndex).type = typeofID(*arg);
                memList(memListIndex).size = sizeof(*arg);

                // Update local key list
                memListKeys(memListIndex) = memList(memListIndex).memoryKey;

                // Update index
                memListIndex++;
                memListCount++;
            }
        }

        arg = next_arg();
        argCounter++;
    } while(numArgs--);
}
//}}}
// DEPRECATED: Delete Array {{{
func cuArrayDeleteDEP(&input)
/* DOCUMENT cuArrayDeleteDEP(*array)
 *
 * DEPRECATED
 *
 * Deletes array on the host and device, while leaving the yorick
 * array intact
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArrayDelete expectes pointer arguments");

    idx = where(memListKeys==cuEncodeToLong(input));

    if (sizeof(idx) && idx(1)) {
        nil = yCudaArrayDelete(YCUDA_DAEMON_PID, input);

        idx = idx(1);
        memListKeys(idx) = 0;
        memList(idx) = memoryItem(0);

        if (idx == memListIndex-1) {
            --memListIndex;
        }

        --memListCount;
    }
}
//}}}
// Delete Array {{{
extern yCudaArrayDelete;
func cuArrayDelete(&input, ..)
/* DOCUMENT cuArrayDelete(*array)
 *
 * Deletes a variadic list of arrays on the host and device, while leaving the 
 * yorick array intact
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    arg = input;
    numArgs = more_args();
    argCounter = 1;

    do {
        if (typeof(arg) != "pointer") {
            nil = write(format="Skippng argument %i as it is not a pointer\n", argCounter);
        } else {
            idx = where(memListKeys==cuEncodeToLong(arg));

            if (sizeof(idx) && idx(1)) {
                nil = yCudaArrayDelete(YCUDA_DAEMON_PID, arg);

                idx = idx(1);
                memListKeys(idx) = 0;
                memList(idx) = memoryItem(0);

                // Could build list of deleted args and free memList items in 
                // bulk in effort to decrement memListIndex
                if (idx == memListIndex-1) {
                    --memListIndex;
                }

                --memListCount;
            }
        }

        arg = next_arg();
        argCounter++;
    } while(numArgs--);
}
//}}}
// FIXME: Set Array {{{
extern yCudaArraySet;
func cuArraySet(&input)
/* DOCUMENT cuArraySet(*array)
 *
 * Updates the values on *array on the device
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArraySet expectes pointer arguments");

    idx = where(memListKeys==cuEncodeToLong(input));

    if (sizeof(idx) && idx(1)) {
        idx = idx(1);

        memList(memListIndex).memoryKey = yCudaArraySet(YCUDA_DAEMON_PID, input, *input);
        memList(memListIndex).dims(1:dimsof(*input)(1)+1) = dimsof(*input);
        memList(memListIndex).type = typeofID(*input);
        memList(memListIndex).size = sizeof(*input);
    }
}
//}}}
// DEPRICATED: Get Array {{{
func cuArrayGetDEP(&input)
/* DOCUMENT cuArrayGet(*array)
 *
 * Returns the data of *array from the device to yorick. The input *array
 * is only used as a key, the results is given as a return value and is not
 * set in place
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArrayGet expectes pointer arguments");

    return yCudaArrayGet(YCUDA_DAEMON_PID, input);
}
//}}}
// Get Array {{{
extern yCudaArrayGet;
func cuArrayGet(&input)
/* DOCUMENT cuArrayGet(*array)
 *
 * Returns the data of *array from the device to yorick. The results are 
 * returned in place
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArrayGet expectes pointer arguments");

    *input = yCudaArrayGet(YCUDA_DAEMON_PID, input);
}
//}}}
// Query Array {{{
extern yCudaArrayQuery;
func cuArrayQuery(&input)
/* DOCUMENT cuArrayQuery(*array)
 *
 * Returns an array containing information on the device data. The
 * returned value contains data type, dimensions and number of elements
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    if (typeof(input) != "pointer")
        error("cuArrayQuery expectes pointer arguments");

    return yCudaArrayQuery(YCUDA_DAEMON_PID, input);
}
//}}}
// Purge Memory {{{
extern yCudaMemoryPurge;
func cuMemoryPurge(nil)
/* DOCUMENT cuMemoryPurge(nil)
 *
 * Purge device and daemon memory completely
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    nil = yCudaMemoryPurge(YCUDA_DAEMON_PID);

    memListKeys(:) = 0;
    memList(:) = memoryItem(0);

    extern memListIndex;
    extern memListCount;
    memListIndex = 1;
    memListCount = 0;
}
//}}}
// Compile Module {{{
extern yCudaModuleCompile;
func cuModuleCompile(modulePath)
/* DOCUMENT yCudaModuleCompile(modulePath)
 *
 * Compiles module given by the argument modulePath (.cu). The path is
 * required to be absolute at the moment
 *
 * TODO:
 *  - Implement relative paths
 *  - Return name and path to ptx file on success
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaModuleCompile(YCUDA_DAEMON_PID, modulePath);
}
//}}}
// Load Module {{{
extern yCudaModuleLoad;
func cuModuleLoad(modulePath)
/* DOCUMENT yCudaModuleLoad(modulePath)
 *
 * Loads module given by modulePath (.cu or .ptx) in to daemon
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaModuleLoad(YCUDA_DAEMON_PID, modulePath);
}
//}}}
// Unload Module {{{
extern yCudaModuleUnload;
func cuModuleUnload(modulePath)
/* DOCUMENT yCudaModuleUnload(modulePath)
 *
 * Unloads module given by modulePath (.cu or .ptx) in to daemon
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaModuleUnload(YCUDA_DAEMON_PID, modulePath);
}
//}}}
// Load Function {{{
extern yCudaFunctionLoad;
func cuFunctionLoad(funcName, modulePath)
/* DOCUMENT yCudaFunctionLoad(funcName, modulePath)
 *
 * Loads module given by funcName from modulePath in to daemon
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaFunctionLoad(YCUDA_DAEMON_PID, funcName, modulePath);
}
//}}}
// Unload Function {{{
extern yCudaFunctionUnload;
func cuFunctionUnload(funcName)
/* DOCUMENT yCudaFunctionUnload(funcName)
 *
 * Unloads function given by funcName
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaFunctionUnload(YCUDA_DAEMON_PID, funcName);
}
//}}}
// Kernel launching {{{
extern yCudaKernelLaunch;
func cuKernelLaunch(kernelDims, funcName, argKeys)
/* DOCUMENT cuKernelLaunch(kernelDims, funcName, argKeys)
 *
 * Launches kernel calling function with dimensions
 * [gridDimX,gridDimY,gridDimZ,blockDimX,blockDimY,blockDimZ]
 * and arguments given by argKeys
 *
 * argKeys *must* be passed in using the cuArgs(..) function
 * as this implements argument encoding for the daemon to
 * interpret.
 *
 * e.g.
 *
 * cuKernelLaunch([64,1,1,128,1,1], vecMul, cuArgs(&a,&b,&c,300))
 *
 * will launch 8192 threads executing the function vecMul with the
 * arguments a, b, c, and 300
 *
 */
{
    if (YCUDA_DAEMON_PID == -1)
        error("yCuda daemon not running");

    return yCudaKernelLaunch(   YCUDA_DAEMON_PID,
            kernelDims,
            funcName,
            argKeys);
    /*
     * Args:
     * YCUDA_DAEMON_PID  - for launch function
     * function name     - for daemon
     * arg pointer array - for daemon
     * grid/block dims   - for daemon
     *
     */
}
//}}}
