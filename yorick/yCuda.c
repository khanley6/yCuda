#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200112L
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>

/* Yorick definitions --------------------------------------------------------*/
#include "yapi.h"

/* Cuda definitions ----------------------------------------------------------*/
/*#include <cuda.h>*/
/*#include <cuda_runtime.h>*/

/* yCuda definitions ---------------------------------------------------------*/
#include "common.hpp"
#include "memory.hpp"

/* Globals -------------------------------------------------------------------*/
long YCUDA_DAEMON_PID;

struct shmHeap sharedHeap;
struct shmCmd  sharedCommand;
volatile struct shmStat sharedStatus;


/* Main body -----------------------------------------------------------------*/

// Misc Functions {{{
void Y_yCudaGetVersion(int argc) {
	/*yCuda_version();*/
}

// Data encoding & decoding {{{
/*typedef union {*/
	/*char c;*/
	/*short s;*/
	/*int i;*/
	/*long l;*/
	/*float f;*/
	/*double d;*/
	/*void* ptr;*/
/*} dataUnion;*/

// Bitwise cast from supported data types to long
// This is required to encode arguments within cuArgs
void Y_encodeToLong(int argc) {
	dataUnion u;

	if (argc != 1)
		y_error("anyToLong takes one argument");

	switch (yarg_typeid(0)) {
		case Y_CHAR:
			u.c = ygets_c(0);
			break;
		case Y_SHORT:
			u.s = ygets_s(0);
			break;
		case Y_INT:
			u.i = ygets_i(0);
			break;
		case Y_LONG:
			u.l = ygets_l(0);
			break;
		case Y_FLOAT:
			u.f = ygets_f(0);
			break;
		case Y_DOUBLE:
			u.d = ygets_d(0);
			break;
		case Y_POINTER:
			u.ptr = ygets_p(0);
			break;
	}

	ypush_long(u.l);
}

void Y_decodeFromLong(int argc) {
	dataUnion u;
	long dims[Y_DIMSIZE] = {1,1,0,0,0,0,0,0,0,0,0};

	char *cPtr;
	short *sPtr;
	int *iPtr;
	long *lPtr;
	float *fPtr;
	double *dPtr;
	long *ptrPtr;

	if (argc != 2)
		y_error("longToAny takes two arguments");

	u.l = ygets_l(1);

	switch (ygets_l(0)) {
		case Y_CHAR:
			cPtr = ypush_c(dims);
			*cPtr = u.c;
			break;
		case Y_SHORT:
			sPtr = ypush_s(dims);
			*sPtr = u.s;
			break;
		case Y_INT:
			iPtr = ypush_i(dims);
			*iPtr = u.i;
			break;
		case Y_LONG:
			lPtr = ypush_l(dims);
			*lPtr = u.l;
			break;
		case Y_FLOAT:
			fPtr = ypush_f(dims);
			*fPtr = u.f;
			break;
		case Y_DOUBLE:
			dPtr = ypush_d(dims);
			*dPtr = u.d;
			break;
		/*case Y_POINTER:*/
			/*ptrPtr = ypush_l(dims);*/
			/**ptrPtr = u.ptr;*/
			/*break;*/
		default:
			y_error("Bad value for data type");
	}
}
//}}}
//}}}

// Shared Memory Functions {{{
// Heap Attach & Detach {{{
static void shmHeapAttach(void) {
	sharedHeap.fd = shm_open(shmHeapFile, O_RDWR, 0600);
	if (sharedHeap.fd == -1) {
		exit(EXIT_FAILURE); // no valid fd
	}

	// Need size of shared memory file
	struct stat st;
	fstat(sharedHeap.fd, &st);

	sharedHeap.size = st.st_size;

	sharedHeap.heap = mmap(NULL, st.st_size, PROT_READ|PROT_WRITE,
			MAP_SHARED, sharedHeap.fd, 0);

	if (sharedHeap.heap == MAP_FAILED) {
		exit(EXIT_FAILURE); // Attach failed
	}

}

static void shmHeapDetach(void) {
	close(sharedHeap.fd);
	munmap(sharedHeap.heap, sharedHeap.size);
}
//}}}
// Command Attach & Detach {{{
static void shmCommandAttach(void) {
	sharedCommand.fd = shm_open(shmCommandFile, O_RDWR, 0600);
	sharedCommand.size = sizeof(struct yCudaCommandStruct);

	if (sharedCommand.fd == -1) {
		exit(EXIT_FAILURE); // no valid fd
	}
	
	sharedCommand.sharedCommandStruct = (struct yCudaCommandStruct*)mmap(
				NULL, sharedCommand.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedCommand.fd, 0);

	if (sharedCommand.sharedCommandStruct == MAP_FAILED) {
		exit(EXIT_FAILURE); // Attach failed
	}

}

static void shmCommandDetach(void) {
	close(sharedCommand.fd);
	munmap(sharedCommand.sharedCommandStruct, sharedCommand.size);
}
//}}}
// Status Attach & Detach {{{
static void shmStatusAttach(void) {
	sharedStatus.fd = shm_open(shmStatusFile, O_RDWR|O_CREAT, 0600);
	sharedStatus.size = sizeof(daemonStatus);

	if (sharedStatus.fd == -1) {
		exit(EXIT_FAILURE); // no valid fd
	}
	
	sharedStatus.status = (daemonStatus*)mmap(
				NULL, sharedStatus.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedStatus.fd, 0);

	if (sharedStatus.status == MAP_FAILED) {
		exit(EXIT_FAILURE); // Attach failed
	}

}

static void shmStatusDetach(void) {
	close(sharedStatus.fd);
	munmap(sharedStatus.status, sharedStatus.size);
}
//}}}
// Shared Memory Resizing {{{
static void checkHeapAndResize(size_t desiredSize) {
    int err;
	// Check if desiredSize is larger than the heap
	if (desiredSize > sharedHeap.size) {
		// if so, unmap the shared memory, resize and remap
		munmap(sharedHeap.heap, sharedHeap.size);
		sharedHeap.size = desiredSize;
		err = ftruncate(sharedHeap.fd, sharedHeap.size);
		sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedHeap.fd, 0);
	}
}

static void checkHeapAndResizeNoTruncate(size_t desiredSize) {
	// Check if desiredSize is larger than the heap
	if (desiredSize > sharedHeap.size) {
		// if so, unmap the shared memory, resize and remap
		munmap(sharedHeap.heap, sharedHeap.size);
		sharedHeap.size = desiredSize;
		sharedHeap.heap = mmap(NULL, sharedHeap.size, PROT_READ|PROT_WRITE,
				MAP_SHARED, sharedHeap.fd, 0);
	}
}
//}}}
//}}} 

// Daemon Functions {{{
// Signals {{{
static void killDaemon(int pid) {
	kill(pid, SIGTERM);
}

static void signalDaemon(int pid) {
	kill(pid, SIGUSR1);
}

static void syncDaemon(int pid) {
	kill(pid, SIGUSR2);
}
//}}}
// Launch Daemon {{{
void Y_yCudaInit(int argc) {
    // FIXME
	/*ycall_on_quit(killDaemon);*/
	ystring_t daemonPath = ygets_q(0);

	pid_t pid;
	int status;

	if ((pid = fork()) == 0) {
		execl(daemonPath, "yCudaDaemon", NULL);
		_exit(127);
	} else if (pid < 0) {
		// TODO: Handle error
		ypush_int(-1);
	} else {
		// FIXME: Need to do _a lot_ more error checking
		long pid_value = -1;

		int pid_fd = shm_open(shmPIDFile, O_RDWR | O_CREAT, 0600);

		int err = ftruncate(pid_fd, sizeof(pid_t));

		pid_t *pid_sh = (pid_t*)mmap(NULL, sizeof(pid_t), PROT_READ|PROT_WRITE,
				MAP_SHARED, pid_fd, 0);

		if (waitpid(pid, &status, 0) > 0)
			pid_value = (long)(*pid_sh); //FIXME: Why is this a bollocks..?
			/*ypush_int(*pid_sh);*/
		
		shmStatusAttach();
		*sharedStatus.status = DAEMON_BUSY;

		while (*sharedStatus.status == DAEMON_BUSY)
			if (*sharedStatus.status == DAEMON_ERROR)
				y_error("Daemon crashed with DAEMON_ERROR status");


		ypush_long(pid_value);

		shmStatusDetach();

		close(pid_fd);
		munmap(pid_sh, sizeof(pid_t));
	}
}
//}}}
// Kill Daemon {{{
void Y_yCudaKill(int argc) {
	int pid = ygets_l(0);
	killDaemon(pid);
}
//}}}
// Sync Daemon {{{
void Y_yCudaSync(int argc) {
	int pid = ygets_l(0);
	syncDaemon(pid);

	shmStatusAttach();

	// Ensure daemon is busy in case it doesn't wake up quickly enough
	*sharedStatus.status = DAEMON_BUSY;

	// Spin thread waiting on daemon status
	while (*sharedStatus.status == DAEMON_BUSY)
		;

	shmStatusDetach();
}
//}}}
// Check daemon {{{
void Y_yCudaPIDs(int argc) {
    FILE *fp;
    char buf[256];
    long pids[64] = {0};
    long outputDims[Y_DIMSIZE] = {1,0};

    const char *cmd = "ps -ef | grep [y]CudaDaemon | awk '{ print $2 }'";

    fp = popen(cmd, "r");
    if (fp == NULL) {
        y_error("Unable to run `ps -ef` command");
    }

    int numPIDs, i = 0;
    while (fgets(buf, sizeof(buf)-1, fp) != NULL) {
        pids[i++] = atol(buf);
    }

    numPIDs = (i==0 ? 1 : i);

    pclose(fp);

    // Return pids to yorick
    outputDims[1] = numPIDs; // Number of pids to return
    long* output = ypush_l(outputDims);

    memcpy(output, pids, numPIDs*sizeof(long));
}
//}}}
//}}}


// Allocation & Deallocation functions {{{
// New {{{
void Y_yCudaArray(int argc) {
	shmCommandAttach();
	shmHeapAttach();
	shmStatusAttach();

	sharedCommand.sharedCommandStruct->cType = MEM_NEW;
	void *inputData;
	long numElements;
	size_t numBytes;
	long inputDims[Y_DIMSIZE] = {0};
	long memoryKey = (long)ygets_p(1);
	YCUDA_DAEMON_PID = ygets_l(2);

	if (argc != 3) y_error("cuArray takes one argument - (array/scalar)");

	int i;

	switch (yarg_typeid(0)) {
		case Y_CHAR:
			inputData = ygeta_c(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = CHAR;
			numBytes = numElements*sizeof(char);
			break;
		case Y_SHORT:
			inputData = ygeta_s(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = SHORT;
			numBytes = numElements*sizeof(short);
			break;
		case Y_INT:
			inputData = ygeta_i(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = INT;
			numBytes = numElements*sizeof(int);
			break;
		case Y_LONG:
			inputData = ygeta_l(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = LONG;
			numBytes = numElements*sizeof(long);
			break;
		case Y_FLOAT:
			inputData = ygeta_f(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = FLOAT;
			numBytes = numElements*sizeof(float);
			break;
		case Y_DOUBLE:
			inputData = ygeta_d(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = DOUBLE;
			numBytes = numElements*sizeof(double);
			break;
		/*case Y_COMPLEX:*/
			/*inputData = ygeta_z(1, NULL, NULL);*/
			/*sharedCommand.sharedCommandStruct->dType = COMPLEX;*/
			/*sharedCommand.sharedCommandStruct->dataSize = numElements*;*/
			/*break;*/
		default:
			shmHeapDetach();
			shmCommandDetach();
			y_error("cuArray::Bad data type");
	}

	for (i = 0; i < Y_DIMSIZE; ++i)
		sharedCommand.sharedCommandStruct->dims[i] =  inputDims[i];

	sharedCommand.sharedCommandStruct->memoryKey   = memoryKey;
	sharedCommand.sharedCommandStruct->numElements = numElements;
	sharedCommand.sharedCommandStruct->dataSize    = numBytes;

	checkHeapAndResize(numBytes);

	// copy inputData to heap
	memcpy(sharedHeap.heap, inputData, numBytes);

	sharedCommand.sharedCommandStruct->dataPtr = sharedHeap.heap;

	// Signal Daemon
	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		;

	shmStatusDetach();
	shmHeapDetach();
	shmCommandDetach();

	ypush_long(memoryKey);
}
//}}}
// Delete {{{
void Y_yCudaArrayDelete(int argc) {
	shmCommandAttach();
	shmStatusAttach();

	long memoryKey = (long)ygets_p(0);
	YCUDA_DAEMON_PID = ygets_l(1);

	if (argc != 2) y_error("cuArrayDelete takes one argument - (array/scalar)");

	sharedCommand.sharedCommandStruct->cType = MEM_DELETE;
	sharedCommand.sharedCommandStruct->memoryKey = memoryKey;

	// Signal Daemon
	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		;

	shmStatusDetach();
	shmCommandDetach();

	ypush_long(memoryKey);
}
//}}}
//}}}

// Other Memory Function {{{
void Y_yCudaMemoryPurge(int argc) {
	shmCommandAttach();
	shmHeapAttach();
	shmStatusAttach();

	sharedCommand.sharedCommandStruct->cType = MEM_PURGE;
	YCUDA_DAEMON_PID = ygets_l(0);

    // This should never be an issue
	if (argc != 1) y_error("cuMemoryPurge only takes YCUDA_DAEMON_PID as an argument");

	// Signal Daemon
	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		;

	shmStatusDetach();
	shmHeapDetach();
	shmCommandDetach();

	ypush_long(1);
}
//}}}

// Memory Getters and Setters {{{
// Setter - FIXME {{{
void Y_yCudaArraySet(int argc) {
	shmCommandAttach();
	shmHeapAttach();

	sharedCommand.sharedCommandStruct->cType = MEM_SET;
	void *inputData;
	long numElements;
	size_t numBytes;
	long inputDims[Y_DIMSIZE] = {0};
	long memoryKey = (long)ygets_p(1);
	YCUDA_DAEMON_PID = ygets_l(2);

	if (argc != 3) y_error("cuArray takes one argument - (array/scalar)");

	int i;

	switch (yarg_typeid(0)) {
		case Y_CHAR:
			inputData = ygeta_c(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = CHAR;
			numBytes = numElements*sizeof(char);
			break;
		case Y_SHORT:
			inputData = ygeta_s(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = SHORT;
			numBytes = numElements*sizeof(short);
			break;
		case Y_INT:
			inputData = ygeta_i(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = INT;
			numBytes = numElements*sizeof(int);
			break;
		case Y_LONG:
			inputData = ygeta_l(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = LONG;
			numBytes = numElements*sizeof(long);
			break;
		case Y_FLOAT:
			inputData = ygeta_f(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = FLOAT;
			numBytes = numElements*sizeof(float);
			break;
		case Y_DOUBLE:
			inputData = ygeta_d(0, &numElements, inputDims);
			sharedCommand.sharedCommandStruct->dType = DOUBLE;
			numBytes = numElements*sizeof(double);
			break;
		/*case Y_COMPLEX:*/
			/*inputData = ygeta_z(1, NULL, NULL);*/
			/*sharedCommand.sharedCommandStruct->dType = COMPLEX;*/
			/*sharedCommand.sharedCommandStruct->dataSize = numElements*;*/
			/*break;*/
		default:
			shmHeapDetach();
			shmCommandDetach();
			y_error("cuArray::Bad data type");
	}

	for (i = 0; i < Y_DIMSIZE; ++i)
		sharedCommand.sharedCommandStruct->dims[i] =  inputDims[i];

	sharedCommand.sharedCommandStruct->memoryKey   = memoryKey;
	sharedCommand.sharedCommandStruct->numElements = numElements;
	sharedCommand.sharedCommandStruct->dataSize    = numBytes;

	checkHeapAndResize(numBytes);

	// copy inputData to heap
	memcpy(sharedHeap.heap, inputData, numBytes);

	sharedCommand.sharedCommandStruct->dataPtr = sharedHeap.heap;

	// Signal Daemon
	signalDaemon(YCUDA_DAEMON_PID);

	shmHeapDetach();
	shmCommandDetach();

	ypush_long(memoryKey);
}
//}}}
// Getter {{{
void Y_yCudaArrayGet(int argc) {
	shmCommandAttach();
	shmHeapAttach();
	shmStatusAttach();

	long returnDims[Y_DIMSIZE] = {0};
	long memoryKey = (long)ygets_p(0);
	YCUDA_DAEMON_PID = ygets_l(1);

	sharedCommand.sharedCommandStruct->cType = MEM_GET;
	sharedCommand.sharedCommandStruct->memoryKey = memoryKey;

	// Signal daemon
	// Ensure daemon is busy in case it doesn't wake up quickly enough
	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	// Spin thread waiting on daemon status
	while (*sharedStatus.status == DAEMON_BUSY)
		;

	// Contruct return values
	void* output;
	
	memcpy(returnDims, sharedCommand.sharedCommandStruct->dims,
			Y_DIMSIZE*sizeof(long));

	switch (sharedCommand.sharedCommandStruct->dType) {
		case CHAR:
			output = ypush_c(returnDims);
			break;
		case SHORT:
			output = ypush_s(returnDims);
			break;
		case INT:
			output = ypush_i(returnDims);
			break;
		case LONG:
			output = ypush_l(returnDims);
			break;
		case FLOAT:
			output = ypush_f(returnDims);
			break;
		case DOUBLE:
			output = ypush_d(returnDims);
			break;
		// FIXME: All these dodgey cases...
		case COMPLEX:
			break;
		case STRING:
			break;
		case POINTER:
			break;
		case STRUCT:
			break;
		case VOID:
			break;
	}

	checkHeapAndResizeNoTruncate(sharedCommand.sharedCommandStruct->dataSize);
	memcpy(output, sharedHeap.heap, sharedCommand.sharedCommandStruct->dataSize);

	shmStatusDetach();
	shmHeapDetach();
	shmCommandDetach();
}
//}}}
// Query {{{
void Y_yCudaArrayQuery(int argc) {
	shmCommandAttach();
	shmHeapAttach();
	shmStatusAttach();

	// Construct command struct with relevant data
	// outputDims[0] = dType
	// outputDims[1] = numElements
	// outputDims[2...] = dims
	long outputDims[Y_DIMSIZE] = {1, Y_DIMSIZE+2};
	long memoryKey = (long)ygets_p(0);
	YCUDA_DAEMON_PID = ygets_l(1);

	sharedCommand.sharedCommandStruct->cType = MEM_QUERY;
	sharedCommand.sharedCommandStruct->memoryKey = memoryKey;

	// Signal daemon
	// Ensure daemon is busy in case it doesn't wake up quickly enough
	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	// Spin thread waiting on daemon status
	while (*sharedStatus.status == DAEMON_BUSY)
		;

	// Contruct return values
	long* output = ypush_l(outputDims);
	output[0] = (long)sharedCommand.sharedCommandStruct->dType;
	output[1] = sharedCommand.sharedCommandStruct->numElements;
	memcpy((output+2), sharedCommand.sharedCommandStruct->dims,
			11*sizeof(long));

	shmStatusDetach();
	shmHeapDetach();
	shmCommandDetach();
}
//}}}
//}}}

// Module and Function functions {{{
// Module Compile {{{
void Y_yCudaModuleCompile(int argc) {
	shmCommandAttach();

	ystring_t modulePath = ygets_q(0);
	YCUDA_DAEMON_PID     = ygets_l(1);

	int numCharsModule = strlen(modulePath) + 1; // include null terminator

	sharedCommand.sharedCommandStruct->cType = MOD_COMPILE;
	memcpy(sharedCommand.sharedCommandStruct->modulePath, modulePath,
			numCharsModule*sizeof(char));

	shmCommandDetach();

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		;

	ypush_long(1);
}
//}}}
// Module Load {{{
void Y_yCudaModuleLoad(int argc) {
	shmCommandAttach();
	shmStatusAttach();

	ystring_t modulePath = ygets_q(0);
	YCUDA_DAEMON_PID     = ygets_l(1);

    /*int numCharsModule = strlen(modulePath);*/
    int numCharsModule = strlen(modulePath) + 1; // Include null

	sharedCommand.sharedCommandStruct->cType = MOD_LOAD;

    // Module name
	memcpy(sharedCommand.sharedCommandStruct->modulePath, modulePath,
			numCharsModule*sizeof(char));
    /*sharedCommand.sharedCommandStruct->modulePath[numCharsModule] = '\0';*/

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		if (*sharedStatus.status == DAEMON_ERROR)
			y_error("Daemon crashed with DAEMON_ERROR status");

	ypush_long(1);

	shmStatusDetach();
	shmCommandDetach();
}
//}}}
// Module Unload {{{
void Y_yCudaModuleUnload(int argc) {
	shmCommandAttach();
	shmStatusAttach();

	ystring_t modulePath = ygets_q(0);
	YCUDA_DAEMON_PID     = ygets_l(1);

	int numCharsModule = strlen(modulePath) + 1; // include null terminator

	sharedCommand.sharedCommandStruct->cType = MOD_UNLOAD;
	memcpy(sharedCommand.sharedCommandStruct->modulePath, modulePath,
			numCharsModule*sizeof(char));

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		if (*sharedStatus.status == DAEMON_ERROR)
			y_error("Daemon crashed with DAEMON_ERROR status");

	ypush_long(1);

	shmStatusDetach();
	shmCommandDetach();
}
//}}}
// Function Load {{{
void Y_yCudaFunctionLoad(int argc) {
	shmCommandAttach();
	shmStatusAttach();

	ystring_t modulePath = ygets_q(0);
	ystring_t funcName   = ygets_q(1);
	YCUDA_DAEMON_PID     = ygets_l(2);

    /*int numCharsModule = strlen(modulePath);*/
    /*int numCharsFunc   = strlen(funcName);*/
    int numCharsModule = strlen(modulePath) + 1; // include null
    int numCharsFunc   = strlen(funcName) + 1; // include null

	sharedCommand.sharedCommandStruct->cType = FUNC_LOAD;
	
    // Module name
    memcpy(sharedCommand.sharedCommandStruct->modulePath, modulePath,
			numCharsModule*sizeof(char));
    /*sharedCommand.sharedCommandStruct->modulePath[numCharsModule] = '\0';*/

    // Function name
	memcpy(sharedCommand.sharedCommandStruct->funcName, funcName,
			numCharsFunc*sizeof(char));
    /*sharedCommand.sharedCommandStruct->funcName[numCharsFunc] = '\0';*/

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		if (*sharedStatus.status == DAEMON_ERROR)
			y_error("Daemon crashed with DAEMON_ERROR status");

	ypush_long(1);

	shmStatusDetach();
	shmCommandDetach();
}
//}}}
// Function Unload {{{
void Y_yCudaFunctionUnload(int argc) {
	shmCommandAttach();
	shmStatusAttach();

	ystring_t funcName = ygets_q(0);
	YCUDA_DAEMON_PID   = ygets_l(1);

	int numCharsFunc = strlen(funcName) + 1; // include null terminator

	sharedCommand.sharedCommandStruct->cType = FUNC_UNLOAD;
	memcpy(sharedCommand.sharedCommandStruct->funcName, funcName,
			numCharsFunc*sizeof(char));

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		if (*sharedStatus.status == DAEMON_ERROR)
			y_error("Daemon crashed with DAEMON_ERROR status");

	ypush_long(1);

	shmStatusDetach();
	shmCommandDetach();
}
//}}}
//}}}

// Kernel launching {{{
void Y_yCudaKernelLaunch(int argc) {
	shmCommandAttach();
	shmHeapAttach();
	shmStatusAttach();

	long argNumber;
	long dims[Y_DIMSIZE];

	long *argKeys      = ygeta_l(0, &argNumber, dims); // kernel arg keys
	ystring_t funcName = ygets_q(1);                   // function name
	long *kernelDims   = ygeta_l(2, NULL, NULL);       // kernel dimensions
	YCUDA_DAEMON_PID   = ygets_l(3);                   // daemon pid

	int numCharsFunc = strlen(funcName) + 1; // include null terminator

	// cType
	sharedCommand.sharedCommandStruct->cType = KERN_LAUNCH;

	// dims - 6 is hardcoded for {grid,block}Dim{X,Y,Z}
	memcpy(sharedCommand.sharedCommandStruct->dims, kernelDims, 6*sizeof(long));

	// numElements
	sharedCommand.sharedCommandStruct->numElements = argNumber;

	// funcName
	memcpy(sharedCommand.sharedCommandStruct->funcName, funcName,
			numCharsFunc*sizeof(char));

	// dPtr
	// formality - The heap should never be this small
	checkHeapAndResize(argNumber*sizeof(long));

	memcpy(sharedHeap.heap, argKeys, argNumber*sizeof(long));
	// use dPtr to point to heap
	sharedCommand.sharedCommandStruct->dataPtr = sharedHeap.heap;

	*sharedStatus.status = DAEMON_BUSY;
	signalDaemon(YCUDA_DAEMON_PID);

	while (*sharedStatus.status == DAEMON_BUSY)
		;

	shmStatusDetach();
	shmCommandDetach();
	shmHeapDetach();

	ypush_long(1);
}
//}}}
