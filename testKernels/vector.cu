// Add vectors {{{
// vecAdd Template {{{
template<typename T>
__device__ void vecAdd(T *in1, T *in2, T *out, long len) {
	int i = (blockIdx.x * blockDim.x) + threadIdx.x;
	if (i < len)
		out[i] = in1[i] + in2[i];
}
//}}}

extern "C" __global__ void vecAdd_c(char *in1, char *in2, char *out, long len) {
	vecAdd<char>(in1, in2, out, len);
}

extern "C" __global__ void vecAdd_s(short *in1, short *in2, short *out, long len) {
	vecAdd<short>(in1, in2, out, len);
}

extern "C" __global__ void vecAdd_i(int *in1, int *in2, int *out, long len) {
	vecAdd<int>(in1, in2, out, len);
}

extern "C" __global__ void vecAdd_l(long *in1, long *in2, long *out, long len) {
	vecAdd<long>(in1, in2, out, len);
}

extern "C" __global__ void vecAdd_f(float *in1, float *in2, float *out, long len) {
	vecAdd<float>(in1, in2, out, len);
}

extern "C" __global__ void vecAdd_d(double *in1, double *in2, double *out, long len) {
	vecAdd<double>(in1, in2, out, len);
}
//}}}

// Multiply vectors {{{
// vecMul Template {{{
template<typename T>
__device__ void vecMul(T *in1, T *in2, T *out, long len) {
	int i = (blockIdx.x * blockDim.x) + threadIdx.x;
	if (i < len)
		out[i] = in1[i] * in2[i];
}
//}}}

extern "C" __global__ void vecMul_c(char *in1, char *in2, char *out, long len) {
	vecMul<char>(in1, in2, out, len);
}

extern "C" __global__ void vecMul_s(short *in1, short *in2, short *out, long len) {
	vecMul<short>(in1, in2, out, len);
}

extern "C" __global__ void vecMul_i(int *in1, int *in2, int *out, long len) {
	vecMul<int>(in1, in2, out, len);
}

extern "C" __global__ void vecMul_l(long *in1, long *in2, long *out, long len) {
	vecMul<long>(in1, in2, out, len);
}

extern "C" __global__ void vecMul_f(float *in1, float *in2, float *out, long len) {
	vecMul<float>(in1, in2, out, len);
}

extern "C" __global__ void vecMul_d(double *in1, double *in2, double *out, long len) {
	vecMul<double>(in1, in2, out, len);
}
//}}}
