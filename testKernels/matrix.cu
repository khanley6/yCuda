// Naive matrix multiplication {{{
// naiveMM Template {{{
template <typename T>
__device__ void naiveMM(T *A, T *B, T *C, 
							long numARows, long numACols,
							long numBRows, long numBCols,
							long numCRows, long numCCols) {
	int tx = (blockDim.x * blockIdx.x) + threadIdx.x;
	int ty = (blockDim.y * blockIdx.y) + threadIdx.y;
	int k;

	if (tx < numCCols && ty < numCRows) {
		for (k = 0; k < numACols; ++k) {
			C[(ty * numCCols) + tx] += 
				A[(ty * numACols) + k] * B[(k * numBCols) + tx];
		}
	}
}
//}}}

extern "C" __global__ void naiveMM_c(char *A, char *B, char *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<char>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void naiveMM_s(short *A, short *B, short *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<short>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void naiveMM_i(int *A, int *B, int *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<int>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void naiveMM_l(long *A, long *B, long *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<long>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void naiveMM_f(float *A, float *B, float *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<float>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void naiveMM_d(double *A, double *B, double *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	naiveMM<double>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

//}}}

// Tiled matrix multiplication {{{
// tiledMM Template {{{
#define TILE_WIDTH 16
template<typename T>
__device__ void tiledMM(T *A, T *B, T *C, 
							long numARows, long numACols,
							long numBRows, long numBCols,
							long numCRows, long numCCols) {
	int bx = blockIdx.x,  by = blockIdx.y;
	int tx = threadIdx.x, ty = threadIdx.y;
	int k = numBCols;
	int m = numARows;
	int n = numACols;

	__shared__ T ds_A[TILE_WIDTH][TILE_WIDTH];
	__shared__ T ds_B[TILE_WIDTH][TILE_WIDTH];

	int row = (by * blockDim.y) + ty;
	int col = (bx * blockDim.x) + tx;
	T cValue = 0.0;

	for (int t = 0; t < ((n-1)/TILE_WIDTH)+1; ++t) {
		if (((t*TILE_WIDTH) + tx < numACols) && (row < numARows)) {
			ds_A[ty][tx] = A[n*row + t*TILE_WIDTH + tx];
		} else {
			ds_A[ty][tx] = 0.0;
		}

		if (((t*TILE_WIDTH) + ty < numBRows) && (col < numBCols)) {
			ds_B[ty][tx] = B[(t*TILE_WIDTH + ty)*k + col];
		} else {
			ds_B[ty][tx] = 0.0;
		}

		__syncthreads();

		for (int i = 0; i < TILE_WIDTH; ++i) {
			cValue += ds_A[ty][i] * ds_B[i][tx];
		}

		__syncthreads();
	}

	if (row < numCRows && col < numCCols) {
		C[(row * k) + col] = cValue;
	}
}
//}}}

extern "C" __global__ void tiledMM_c(char *A, char *B, char *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<char>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void tiledMM_s(short *A, short *B, short *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<short>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void tiledMM_i(int *A, int *B, int *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<int>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void tiledMM_l(long *A, long *B, long *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<long>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void tiledMM_f(float *A, float *B, float *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<float>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}

extern "C" __global__ void tiledMM_d(double *A, double *B, double *C,
										long numARows, long numACols,
										long numBRows, long numBCols,
										long numCRows, long numCCols) {
	tiledMM<double>(A, B, C, 
					numARows, numACols,
					numBRows, numBCols,
					numCRows, numCCols);
}
//}}}
