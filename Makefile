CC = g++

CPPFLAGS = -I./include -I/usr/local/cuda/include -std=c++11 -g
LDFLAGS = -L/usr/local/cuda/lib64 -lcuda -lrt

SOURCES = src/daemon.cpp src/memory.cpp src/modules.cpp src/functions.cpp
OBJ = daemon.o memory.o modules.o functions.o

all: yCudaDaemon

debug: CPPFLAGS += -g -fno-omit-frame-pointer -DNOFORK
debug: yCudaDaemon


%.o: src/%.cpp
	$(CC) $< -c $< $(CPPFLAGS) $(LDFLAGS)

yCudaDaemon: $(OBJ)
	$(CC) -o $@ $^ $(CPPFLAGS) $(LDFLAGS)

clean:
	rm ./*.o
	rm ./yCudaDaemon
